import { api } from "../utils/api";

// service for the users
export const FETCH_ALL_USERS = async () => {
  let response = await api.get("users");
  return response.data 
};
